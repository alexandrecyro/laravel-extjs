<?php
$uri   = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$paths = require __DIR__.'/bootstrap/paths.php';

if ($uri !== '/' and file_exists($paths['public'].$uri)) {
   return false;
}

require_once $paths['public'].'/index.php';