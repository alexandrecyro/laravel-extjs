Ext.Loader.setConfig({	
	enabled         : true
	,disableCaching : true
	,paths          : {
		'Ext'     : '/laravel_extjs/public/extjs/src'
		,'Ext.ux' : '/laravel_extjs/public/extjs/ux'		
	}
});

Ext.application({
    name         : 'LaraExt'
	,appFolder   : '/laravel_extjs/public/app'
    ,controllers : [
		'Funcionario'
	]
    ,autoCreateViewport : true
});