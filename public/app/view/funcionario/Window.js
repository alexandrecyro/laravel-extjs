Ext.define('LaraExt.view.funcionario.Window', {
    extend        : 'Ext.window.Window'
    ,requires     : ['Ext.form.Panel','Ext.form.field.Text']
    ,alias        : 'widget.funcionariowindow'
	,iconCls      : 'icon-btn-funcionario'
	,title        : 'Cadastro de Funcionarios'
	,layout       : 'fit'
	,constrain    : true
	,width        : 500
	,bodyPadding  : '5 5 0'
	,border       : false
	,resizable    : false
	,modal        : true
	,autoShow     : true
	,defaultFocus : 'nome'

    ,initComponent: function() {
		this.items = [
			Ext.widget('funcionarioform')
		];

        this.callParent(arguments);
    }

	,buttonAlign : 'center'
	,buttons     : [
		{
			itemId   : 'btnSave'
			,iconCls : 'icon-btn-save'
			,text    : '<b>Salvar</b>'
			,action  : 'save'
		}

		,{
			itemId   : 'btnCancel'
			,iconCls : 'icon-btn-cancel'
			,text    : '<b>Cancelar</b>'
			,action  : 'cancel'
		}
	]
});