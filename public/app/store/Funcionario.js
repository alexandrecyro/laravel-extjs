Ext.define('LaraExt.store.Funcionario', {
    extend      : 'Ext.data.Store'
    ,model      : 'LaraExt.model.Funcionario'
    ,pageSize   : 20
	,autoSync   : true
	,remoteSort : true
    
	,proxy: {
		type            : 'ajax'
		,noCache        : false
		,simpleSortMode : true
		,actionMethods  : {
			create   : 'POST'
			,read    : 'POST'
			,update  : 'POST'
			,destroy : 'POST'
		}
		,api  : {
			create   : 'funcionario/create'
			,read    : 'funcionario/read'
			,update  : 'funcionario/update'
			,destroy : 'funcionario/destroy'
		}

		,reader: {
			type             : 'json'
			,root            : 'data'
			,totalProperty   : 'count'
			,successProperty : 'success'
		}

		,writer: {
			type             : 'json'
			,messageProperty : 'message'
			,root            : ''
			,encode          : false
			,idProperty      : 'id'
			,writeAllFields  : true
			,allowSingle     : true
		}

		,listeners: {
			exception: function(proxy, response, operation) {
				Ext.StoreMgr.lookup('Funcionario').rejectChanges();

				var	obj = Ext.decode(response.responseText);

				Ext.create('widget.uxNotification', {
					title    : 'Aviso!'
					,iconCls : 'ux-notification-icon-exclamation'
					,items   : [
						{
							xtype   : 'component'
							,autoEl : {
								tag  : 'img'
								,src : '/laravel_extjs/public/images/error.png'
							}
						}
						,{
							xtype   : 'component'
							,autoEl : {
								tag    : 'h3'
								,html  : obj.message
								,style : {
									color : '#15498B'
									,'font-family': 'Times New Roman'
									,'font-style': 'italic'
								}
							}
						}
					]
				});
			}
		}
    }

	,listeners  : {
		write: function(store, operation) {
			var	obj = Ext.decode(operation.response.responseText);

			if(obj.success) {
				store.load();

				Ext.create('widget.uxNotification', {
					title    : 'Aviso!'
					,iconCls : 'ux-notification-icon-information'
					,items   : [
						{
							xtype   : 'component'
							,autoEl : {
								tag  : 'img'
								,src : '/laravel_extjs/public/images/good.png'
							}
						}
						,{
							xtype   : 'component'
							,autoEl : {
								tag    : 'h3'
								,html  : obj.message
								,style : {
									color : '#15498B'
									,'font-family': 'Times New Roman'
									,'font-style': 'italic'
								}
							}
						}
					]
				});
			}
		}
    }
});