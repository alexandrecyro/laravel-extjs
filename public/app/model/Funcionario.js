Ext.define('LaraExt.model.Funcionario', {
    extend  : 'Ext.data.Model'
    ,fields : [
		{name: 'id', type: 'int'}
		,{name: 'nome', type: 'string'}
		,{name: 'data_nascimento', type: 'date', dateFormat: 'Y-m-d', dateWriteFormat: 'Y-m-d'}
		,{name: 'cpf', type: 'int'}
		,{name: 'sexo', type: 'string'}
		,{name: 'salario', type: 'float'}
		,{
			name: 'beneficio'
			,type: 'string'
 			,convert:function(v, record){
                if(typeof v == "string")
                    return v.split(",");
                  else
                     return v;
                }
        }
		,{name: 'ativo', type: 'boolean', defaultValue: false}
	]
});