
Ext.define('LaraExt.controller.Funcionario', {
    extend    : 'Ext.app.Controller'
    ,requires : [
        'Ext.ux.window.Notification'
        ,'Ext.ux.TextMaskPlugin'
        ,'Ext.ux.grid.FiltersFeature'
    ]
    ,refs     : [{ref: 'funcionariogrid', xtype: 'funcionariogrid' }]
    ,stores   : ['Funcionario']
    ,models   : ['Funcionario']
    ,views    : [
        'funcionario.Form'
        ,'funcionario.Combo'
        ,'funcionario.Grid'
        ,'funcionario.Window'
    ]

    ,features: [
        {
            id: 'salario',
            itemId:'salario',
            ftype: 'summary',
            dock: 'bottom'
        }
    ]

    ,init: function() {
        var me = this;

        me.control({
            'funcionariogrid': {
                render           : this.funcionarioRender
                ,selectionchange : this.funcionarioSelectionChange
            }
            ,'funcionariogrid button[action=create]': {
                click: this.funcionarioBtnCreate
            }
            ,'funcionariogrid button[action=update]': {
                click: this.funcionarioBtnUpdate
            }
            ,'funcionariogrid button[action=destroy]': {
                click: this.funcionarioBtnDestroy
            }
            ,'funcionariogrid button[action=clear]': {
                click: this.funcionarioBtnClearFilters
            }
            ,'funcionariogrid actioncolumn': {
                click: this.funcionarioActionClick
            }
            ,'funcionariowindow button[action=save]': {
                click: this.funcionarioBtnSave
            }
            ,'funcionariowindow button[action=cancel]': {
                click: this.funcionarioBtnCancel
            }
        });
    }

    ,funcionarioRender: function() {
        this.getFuncionarioStore().on({
            write : this.funcionarioDeselectAll
        });

        this.getFuncionarioStore().load();
    }

    ,funcionarioDeselectAll: function() {
        var grid = Ext.ComponentQuery.query('funcionariogrid')[0];
        grid.getSelectionModel().deselectAll();
    }

    ,funcionarioSelectionChange: function(selModel, selected) {
        var grid = selModel.view.ownerCt;

        grid.down('#btnDestroy').setDisabled(!selected.length);
        grid.down('#btnUpdate').setDisabled(!(selected.length == 1));
        grid.getView().refresh();
    }

    ,funcionarioBtnCreate: function(button) {
        Ext.widget('funcionariowindow', {animateTarget:button.el});      
    }

    ,funcionarioBtnUpdate: function(button) {
        var grid   = button.up('grid');
        var view   = Ext.widget('funcionariowindow', {animateTarget:button.el});
        var record = grid.getSelectionModel().getSelection()[0];
        view.down('form').loadRecord(record);
    }

    ,funcionarioBtnDestroy: function(button) {
        Ext.MessageBox.confirm({
            title          : 'Aviso!'
            ,animateTarget : button.el
            ,msg           : 'Deseja apagar esse registro(s)?'
            ,buttons       : Ext.MessageBox.YESNO
            ,icon          : 'icon-window-question'
            ,scope         : this
            ,fn            : function(btn) {
                if(btn === 'yes') {
                    var store  = button.up('grid').getStore();
                    var record = button.up('grid').getSelectionModel().getSelection();

                    store.getProxy().getWriter().writeAllFields = false;
                    store.getProxy().getWriter().allowSingle    = false;

                    store.remove(record);

                    store.getProxy().getWriter().writeAllFields = true;
                    store.getProxy().getWriter().allowSingle    = true;
                }
            }
        });
    }

    ,funcionarioActionClick: function(view,cell,row,col,e) {
        var rec   = this.getFuncionarioStore().getAt(row);
        var id    = rec.get('id');
        var ativo = !rec.get('ativo');

        rec.set({'id':id, 'ativo': ativo});
    }

    ,funcionarioBtnClearFilters: function(button) {
        this.funcionarioDeselectAll();
        button.up('grid').filters.clearFilters();
    }

    ,funcionarioBtnSave: function(button) {
        var win    = button.up('window'),
            store  = this.getFuncionarioStore(),
            form   = win.down('form'),
            record = form.getRecord(),
            values = form.getValues(false, false, false, true);

        if(form.isValid()) {
            if (!record) {
                record = Ext.create(store.model.modelName);
                record.set(values);
                store.add(record);

            } else {
                record.set(values);
            }

            win.close();
        } else {
            Ext.MessageBox.alert({
                title          : 'Aviso!'
                ,animateTarget : button.el
                ,msg           : 'Digite os dados corretamente!'
                ,buttons       : Ext.MessageBox.OK
                ,icon          : Ext.MessageBox.INFO
            });
        }
    }

    ,funcionarioBtnCancel: function(button) {
        button.up('window').close();
    }
});