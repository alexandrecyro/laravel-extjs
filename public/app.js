Ext.Loader.setConfig({
	enabled         : true
	,disableCaching : true
	,paths          : {
		'Ext.ux' : '/laravel_extjs/public/extjs/ux'
	}
});

Ext.application({
    name         : 'LaraExt'
	,appFolder   : '/laravel_extjs/public/app'
    ,controllers : [
		'Funcionario'
	]
    ,autoCreateViewport : true
});

Ext.define('NoExponent', {
    override: 'Ext.form.field.Number',
    allowExponential: false    
});

Ext.util.Format.brMoney = function(v) {
    v = Ext.num(v, 0);
    v = (Math.round((v - 0) * 100)) / 100;
    v = (v == Math.floor(v)) ? v + ".00" :
    ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
    v = String(v);

    var ps    = v.split('.');
    var whole = ps[0];
    var sub   = ps[1] ? ','+ ps[1] : ',00';
    var r     = /(\d+)(\d{3})/;
    while (r.test(whole)) {
        whole = whole.replace(r, '$1' + '.' + '$2');
    }

    v = whole + sub;

    if (v.charAt(0) == '-') {
        return '-R$ ' + v.substr(1);
    }
    return 'R$ ' + v;
}

Ext.apply(Ext.form.VTypes,{
    cpf: function(val,field){
        if (val!='') {
            if((val = val.replace(/[^\d]/g,"").split("")).length != 11) return false;
            if(new RegExp("^" + val[0] + "{11}$").exec(val.join(""))) return false;
            for(var s = 10, n = 0, i = 0; s >= 2; n += val[i++] * s--);
            if(val[9] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;
            for(var s = 11, n = 0, i = 0; s >= 2; n += val[i++] * s--);
            if(val[10] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;
            return true;
        } else {
            return true;
        }
    },
    cpfText: 'CPF informado é inválido!',
    cpfMask: /[0-9]/i
});